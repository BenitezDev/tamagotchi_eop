package com.benitezdev.tamagotchi;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class CountdownService extends Service {


    private static final String TAG=CountDownTimer.class.getCanonicalName();

    // He leido que asi se puede conseguir un ID unico
    private AtomicInteger c = new AtomicInteger(0);
    public int NOTIFICATION_ID = c.incrementAndGet();


    private static final String CHANNEL_ID = "Notificaciones";
    private static final CharSequence name = "Noti";

    private String [] mensajes = {
            "Tu Tamagotchi necesita que le des de comer",
            "Tu Tamagotchi requiere de tu ayuda para ir al baño",
            "Tu Tamagotchi quiere que le des amor antes de irse a dormir"
    };



    public CountdownService()
    {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(TAG,"onStartCommand");
        new Contador(5000,1000).start();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    class Contador extends CountDownTimer
    {
        Contador(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
            Log.d(TAG,"millisInFuture: "+millisInFuture+" ,, countDownInterval: "+countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
            Log.d(TAG,"onTick. millisUntilFinished: "+millisUntilFinished);
        }

        @Override
        public void onFinish()
        {
            Log.d(TAG,"onFinish");
            // Comprobamos si el dispositivo tiene una versión igual o superior 26
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                crear_notificacion();
            else{
                Log.d(TAG, "Abre la Activity para  android >= 26");
                Intent intent = new Intent(CountdownService.this, MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    /**
     * Función que crea una notificacion para dispositivos con API >= 26
     */
    public void crear_notificacion()
    {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);

        Random rnd = new Random();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Creación de la notificación
        NotificationCompat.Builder notification = new NotificationCompat.Builder(CountdownService.this, CHANNEL_ID)
                .setAutoCancel(true).setContentTitle("Tamagotchi")
                .setWhen(System.currentTimeMillis())
                .setContentText(mensajes[rnd.nextInt(2)])
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setSmallIcon(R.drawable.logo_caca)
                .setContentIntent(pendingIntent);

        notification.build();

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (nm != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            Log.d(TAG, "Lanza notificación android >= 26");
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            nm.createNotificationChannel(mChannel);
            nm.notify(NOTIFICATION_ID, notification.build());
        }


    }
}

