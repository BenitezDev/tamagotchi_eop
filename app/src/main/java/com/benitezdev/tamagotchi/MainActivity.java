package com.benitezdev.tamagotchi;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


/**

 * Esta clase controla el comportamiento de la mascota virtual

 * @author: Alejandro Benítez López

 */
public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    /*
        Breve diseño del juego

        >> Distintas habitaciones con sus identificadores

           Comedor    -> Comer      -> 0
           Baño        -> Defecar    -> 1
           Dormitorio  -> Descansar  -> 2



        >> Cuanta salud suben las opciones de compra:

           Opción barata   -> Cuesta 5     -> Restaura 3

           Opción media     -> Cuesta 10    -> Restaura 10

           Opción gourmet  -> Cuesta 15    -> Restaura 25

        >> Nota: Soy programador, no diseñador

     */

    //  T0DO se gestiona en esta misma clase, todas las variables son privadas.

    // FONDOS:
    private int indice_habitacion = 0 ;             /** Guarda el índice de la habitación actual*/
    private FrameLayout fondo_layout  ;             /** Guarda el Layout principal*/
    private int[] fondos_id = new int[]
    {
            R.drawable.comedor  ,
            R.drawable.bathroom ,
            R.drawable.dormitorio
    };                                              /** Array con los identificadores de los drawables para cada habitación*/


    // ESTADÍSTICAS. BARRAS DE PORCENTAJES
    private int [] stats_number = new int[3];       /** Array que guarda el valor actual de cada una de las barras necesidades*/
    private int [] img_porcentaje_id = new int[]
    {
            R.id.img_porcentaje_comida ,
            R.id.img_porcentaje_caca   ,
            R.id.img_porcentaje_dormir
    };                                               /** Array que guarda los identicadores de las barras de cada necesidad*/


    // ESTADOS DE LA MASCOTA VIRTUAL
    private enum Estados
    {
        FELIZ, NORMAL, TRISTE, MUERTO
    }                                               /** Enumeración de todos los estados posibles de la mascota*/
    private Estados mi_estado;                      /** Estado actual de la mascota*/
    private int calculo_estado = 0;                 /** Variable que representa numericamente el estado global de la mascota*/


    // VARIABLES PARA REFERENCIAS A ELEMENTOS DEL .XML
    private ImageButton flecha_izq, flecha_dcha;    /** Guarda los ImageButton de las flechas para cambiar de habitación*/
    private TextView nombre_habitacion;             /** Guarda el TextView donde pondrá el nombre de la habitación actual*/

    private ImageButton avatar;                     /** Guarda el ImageButton de la representacion gráfica de la mascota*/

    private TextView dinero_text;                   /** Guarda el TextView donde se viasualizará la cantidad de dinero que tengas*/
    private TextView dinero_compra;                 /** Guarda el TextView donde se viasualizará la cantidad de
                                                        dinero que tengas en el apartado de compra*/
    private ImageButton
            primera_opcion_compra,
            segunda_opcion_compra,
            tercera_opcion_compra;                  /** Guarda los ImageButton de las opciones de compra */


    // ANIMACIÓN DE LA MASCOTA
    private int[][] avatar_frame = new int[][]
    {
            // Estado feliz
            {R.drawable.cara_feliz_1,R.drawable.cara_feliz_2},
            //Estado normal
            {R.drawable.cara_normal_1,R.drawable.cara_normal_2},
            //Estado triste
            {R.drawable.cara_triste_1,R.drawable.cara_triste_2},
            //Estado muerto
            {R.drawable.cara_muerto_1,R.drawable.cara_muerto_2},
    };                                              /** Array bidimensional que guarda los identificadores a los drawables de
                                                        los frames de la animación de cada uno de los estados de la mascota*/
    private boolean is_frame_1 = true;              /** Booleano que es true cuando esta el primer frame activo */

    // DINERO / ECONOMÍA
    private int dinero;                             /** Variable donde se guardara la cantidad actual de dinero*/
    private int[] precios = new int[]
    {
            5,10,15
    };                                              /** Array con los precios de las opciones de compra */
    private int [] cuanto_restaura_cada_cosa = new int[]
    {
            10,20,30
    };                                              /** Array con lo que restaura cada opcion de compra*/
    private int[][] id_opciones_compra = new int[][]
    {
            { R.drawable.comida_malo,   R.drawable.comida_normal,   R.drawable.comida_bueno },
            { R.drawable.papel_malo,    R.drawable.papel_normal,    R.drawable.papel_bueno},
            { R.drawable.almohada_malo, R.drawable.almohada_normal, R.drawable.almohada_bueno }
    };                                              /** Array bidimensional que guardara los identificadores de los drawables de
                                                        cada una de las opciones de compra en cada habitación*/

    // HABITACIONES
    private String [] nombres_habitaciones = new String[]
    {
            "Comedor","Baño","Dormitorio"
    };                                              /** Array que guarda los nombres de las habitaciones*/


    //DOBLE CLICK
    /**
     * Método que se ejecuta cuando minimizamos o cerramos la aplicación
     */
    protected void onPause() {
        super.onPause();

        // Creamos un intent para utilizar el servicio CountdownService y hacer la cuenta atrás
        Intent intent = new Intent(getBaseContext(),CountdownService.class);
        Log.d(TAG, "Lanzamos servicio CountdownService");
        // Se lanza el intent
        startService(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Llamamos a esta funcion para inicializar todas las variables
        initialization();

        // Hacemos una habitación visible para empezar a jugar
        set_room_visible(indice_habitacion = 1);

        // "Bucle Update"
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                handler.postDelayed(this, 250);
                Random rnd = new Random();
                // De forma aleatoria se elige que estadística se va a decrementar
                int que_stats_va_a_cambiar = rnd.nextInt(3);

                // y se decrementa
                decrement_stats(que_stats_va_a_cambiar,5);

                // Se calcula cual es el estado global.
                calculo_estado = 0; // Sera un valor del 0 hasta 300 (3 estadisticas X 100)
                for(int i = 0; i < stats_number.length; ++i)
                {
                    // Vamos sumando las estadisticas hasta que se salga del bucle
                    calculo_estado +=stats_number[i];
                    // Aprovechamos el buche y comprobamos el color de las barras
                    check_stats_color(i);
                }
                // Calculamos el estado de la mascota
                calcular_estado();
                // Hacemos la animación correspondiente al estado
                change_frame();

            }
        },5000);

        /**
         * Añade un listener al ImageButton de la flecha izquierda y le añade la funcionalidad
         * de cambiar entre las habitaciones
         */
        flecha_izq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(MainActivity.this, "Pulsa izquierda", Toast.LENGTH_SHORT).show();

                if(indice_habitacion <= 0)
                    set_room_visible(indice_habitacion = 2);
                else set_room_visible(--indice_habitacion);
            }
        });

        /**
         * Añade un listener al ImageButton de la flecha derecha y le añade la funcionalidad
         * de cambiar entre las habitaciones
         */
        flecha_dcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(MainActivity.this, "Pulsa derecha", Toast.LENGTH_SHORT).show();

                if(indice_habitacion >= 2)
                    set_room_visible(indice_habitacion = 0);
                else
                    set_room_visible(++indice_habitacion);
            }
        });

        /**
         * Añade un listener al ImageButton de la mascota. Sirve para conseguir dinero
         */
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_money();
            }
        });

        /**
         * Añade un listener al ImageButton de la primera opcion de compra. Da la funcionalidad
         *
         * de que si se tiene el dinero suficiente para comprar esta opción la compra.
         */
        primera_opcion_compra.setOnClickListener(new View.OnClickListener() {
            private long lastTouchTime = 0;
            private long currentTouchTime = 0;

            @Override
            public void onClick(View view) {
                lastTouchTime = currentTouchTime;
                currentTouchTime = System.currentTimeMillis();

                if (currentTouchTime - lastTouchTime < 250)
                {
                    lastTouchTime = 0;
                    currentTouchTime = 0;
                    if(dinero >= precios[0])
                    {
                        // Quitamos el dinero
                        dinero -= precios[0];
                        // Actualizamos el dinero, ya no será el mismo porque se le ha restado el coste de lo que ha comprado
                        show_money(dinero);
                        // Actualizamos graficamente el estado de las barras
                        improve_stats(0); // se le pasa como parametro la opcion que ha comprado
                    }
                }

            }
        });

        /**
         * Añade un listener al ImageButton de la segunda opción de compra. Da la funcionalidad
         *
         * de que si se tiene el dinero suficiente para comprar esta opción la compra.
         */
        segunda_opcion_compra.setOnClickListener(new View.OnClickListener() {
            private long lastTouchTime = 0;
            private long currentTouchTime = 0;

            @Override
            public void onClick(View view) {
                lastTouchTime = currentTouchTime;
                currentTouchTime = System.currentTimeMillis();

                if (currentTouchTime - lastTouchTime < 250)
                {
                    lastTouchTime = 0;
                    currentTouchTime = 0;
                    if(dinero >= precios[1])
                    {
                        // Quitamos el dinero
                        dinero -= precios[1];
                        // Actualizamos el dinero, ya no será el mismo porque se le ha restado el coste de lo que ha comprado
                        show_money(dinero);
                        // Actualizamos graficamente el estado de las barras
                        improve_stats(1); // se le pasa como parametro la opcion que ha comprado
                    }
                }

            }
        });


        /**
         * Añade un listener al ImageButton de la tercera opción de compra. Da la funcionalidad
         *
         * de que si se tiene el dinero suficiente para comprar esta opción la compra.
         */
       tercera_opcion_compra.setOnClickListener(new View.OnClickListener() {
           private long lastTouchTime = 0;
           private long currentTouchTime = 0;

           @Override
           public void onClick(View view) {
               lastTouchTime = currentTouchTime;
               currentTouchTime = System.currentTimeMillis();

               if (currentTouchTime - lastTouchTime < 250)
               {
                   lastTouchTime = 0;
                   currentTouchTime = 0;
                   if(dinero >= precios[2])
                   {
                       // Quitamos el dinero
                       dinero -= precios[2];
                       // Actualizamos el dinero, ya no será el mismo porque se le ha restado el coste de lo que ha comprado
                       show_money(dinero);
                       // Actualizamos graficamente el estado de las barras
                       improve_stats(2); // se le pasa como parametro la opcion que ha comprado
                   }
               }

           }
       });



    }


    /**
     * Método que hace visible una habitación con todos sus elementos
     * @param index_room indice de la habitacion que se quiere hacer visible
     */
    private void set_room_visible(int index_room)
    {
        // Cambia el fondo principal al drawable que corresponda
        fondo_layout.setBackground(getDrawable(fondos_id[index_room]));
        // Cambia el nombre de la habitacion
        nombre_habitacion.setText(nombres_habitaciones[index_room]);
        // Actualiza las opciones de compra
        primera_opcion_compra.setImageResource(id_opciones_compra[index_room][0]);
        segunda_opcion_compra.setImageResource(id_opciones_compra[index_room][1]);
        tercera_opcion_compra.setImageResource(id_opciones_compra[index_room][2]);

    }

    /**
     * Método que inicializa las variables del juego
     */
    private void initialization()
    {
        fondo_layout = (FrameLayout) findViewById(R.id.fondo);
        nombre_habitacion = findViewById(R.id.nombre_habitacion_actual);
        primera_opcion_compra = (ImageButton) findViewById(R.id.primera_opcion_compra);
        segunda_opcion_compra = (ImageButton) findViewById(R.id.segunda_opcion_compra);
        tercera_opcion_compra = (ImageButton) findViewById(R.id.tercera_opcion_compra);

        flecha_izq = (ImageButton) findViewById(R.id.flecha_izq);
        flecha_dcha = (ImageButton) findViewById(R.id.flecha_dcha);

        dinero = 0;
        dinero_text = (TextView) findViewById(R.id.dinero);
        dinero_text.setText("0€");
        dinero_compra = (TextView) findViewById(R.id.dinero_compra);
        dinero_compra.setText("0€");

        avatar = (ImageButton) findViewById(R.id.avatar);

        for (int i = 0; i < img_porcentaje_id.length ; ++i)
        {
            /* Empezar el juego con valores aleatorios en las estadísticas
            Random rand = new Random();
            stats_number[i] =  rand.nextInt(101); // Random del 0 hasta 100 (ambos incluidos)
            convert_from_percentage_to_dp(i,stats_number[i]);
            */

            // Creo que es mejor empezar con las estadísticas llenas
            stats_number[i] = 100;
            convert_from_percentage_to_dp(i,stats_number[i]);
        }
    }

    /**
     * Método que convierte de un porcentaje (0~100) a dp
     * @param index Indice de la barra de estadisticas que se quiere modificar
     * @param percentage Que porcentaje se quiere rellenar
     */
    private void convert_from_percentage_to_dp(int index, int percentage )
    {
        /*
               52 dp    ->      100%
               x  dp    ->      percentage
         */

        // Hacemos una regla de tres
        float new_dp = (percentage*52)/100;
        // Hacemos una variable que guarde el valor proporcional de px a pd
        float dp_to_px = convertDpToPixel(new_dp, findViewById(img_porcentaje_id[index]).getContext());

        // Cambiamos la altura de la barra a los dp correspondientes
        findViewById(img_porcentaje_id[index]).getLayoutParams().height = (int) dp_to_px;
        // Hay que hacer esto porque, si no, no funciona. stackoverflow.com sabrá por qué
        findViewById(img_porcentaje_id[index]).requestLayout();
    }


    /**
     * Gracias stackoverflow.com!
     *
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    private float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);

        return px;
    }


    /**
     * Método que suma una unidad a la cantidad de dinero actual
     */
    private void get_money()
    {
        // Suma uno de dinero
        dinero++;
        // Actualiza los contadores visuales del dinero
        show_money(dinero);
    }


    /**
     * Método que actualiza el valor visual del dinero
     * @param dinero dinero que se quiere actualizar
     */
    private void show_money(int dinero) // Da igual pasarle un parámetro, estamos en la misma clase
    {
        // Actualizamos el contador de dinero de la esquina superior izquierda
        dinero_text.setText(dinero+"€");
        // Actualizamos el contador de dinero de la opción de compra
        dinero_compra.setText(dinero+"€");
    }


    /**
     * Método que cambia el color de la barra de estados dependiendo del valor
     * @param index Indice de la barra que se va a comprobar
     */
    private void check_stats_color(int index)
    {
        if(stats_number[index] >= 75)
        {
            findViewById(img_porcentaje_id[index]).setBackgroundResource(color.holo_green_dark);
        }
        else if(stats_number[index] >= 50 && stats_number[index] < 75)
        {
            findViewById(img_porcentaje_id[index]).setBackgroundResource(color.holo_green_light);
        }
        else if(stats_number[index] >= 25 && stats_number[index] < 50)
        {
            findViewById(img_porcentaje_id[index]).setBackgroundResource(color.holo_orange_light);
        }
        else if(stats_number[index] < 25)
        {
            findViewById(img_porcentaje_id[index]).setBackgroundResource(color.holo_red_dark);
        }
    }

    /**
     * Método que actualiza la representación gráfica de las estadisticas. aka Las barras de arriba de la pantalla
     * @param opcion_comprada Indice de la barra que debe actualizar
     */
    private void improve_stats(int opcion_comprada)
    {
        // Sumamos el valor que tiene la barra más lo que debería subir
        int suma = stats_number[indice_habitacion]+cuanto_restaura_cada_cosa[opcion_comprada];

        // Comprobamos si supera los limites
        if(suma < 100)
        {
            // Actualizamos el valor de la estadistica
            stats_number[indice_habitacion] = suma;
            // Actualizamos el color de la barra
            check_stats_color(indice_habitacion);
            // Convertimos de pixeles a dp
            convert_from_percentage_to_dp(indice_habitacion,stats_number[indice_habitacion]);
        }
        else
        {
            // Actualizamos el valor de las estadísticas al máximo permitido
            stats_number[indice_habitacion] = 100;
            // Actualizamos el color de la barra
            check_stats_color(indice_habitacion);
            // Convertimos de pixeles a dp
            convert_from_percentage_to_dp(indice_habitacion,stats_number[indice_habitacion]);
        }
    }

    /**
     * Método que decrementa las barras de estadísticas
     * @param index que barra se quiere decrementar
     * @param amount que valor se quiere decrementar
     */
    public void decrement_stats(int index, int amount)
    {
        // Si la barra esta vacía, no decrementamos
        if(stats_number[index] <= 0)
            stats_number[index] = 0;
        else // Si la barra no esta vacía le quitamos tanto como amount diga
            stats_number[index] -= amount;

        // Convertimos de pixeles a dp
       convert_from_percentage_to_dp(index,stats_number[index]);
    }

    /**
     * Método que calcula el estado de la mascota virtual
     */
    private void calcular_estado()
    {
        /*

            300
            |
            |   Feliz
            |
            200
            |
            |   Normal
            |
            100
            |
            |   Triste
            |
            0   Muerto

         */
        if(calculo_estado >= 200 )
        {
            mi_estado = Estados.FELIZ;
        }else
        if (calculo_estado >= 100 )
        {
            mi_estado = Estados.NORMAL;
        }else
        if( calculo_estado > 0 )
        {
            mi_estado = Estados.TRISTE;
        }
        else
        {
            mi_estado = Estados.MUERTO;
        }

        Log.d(TAG,mi_estado + "->" + calculo_estado);
    }

    /**
     * Método que hace la animacion de la mascota dependiendo de su estado
     */
    private void change_frame()
    {
        switch (mi_estado)
        {
            // Comprobamos el estado
            case FELIZ:

                // Comprobamos si estamos en el primer frame de la animación
                if(is_frame_1)
                    // Cambiamos el frame de la animación al otro
                    avatar.setImageResource(avatar_frame[0][0]);
                else
                    // Cambiamos el frame de la animación al otro
                    avatar.setImageResource(avatar_frame[0][1]);

                // Invertimos el booleano
                is_frame_1 = !is_frame_1;
                break;

            case NORMAL:
                if(is_frame_1)
                    avatar.setImageResource(avatar_frame[1][0]);
                else
                    avatar.setImageResource(avatar_frame[1][1]);

                is_frame_1 = !is_frame_1;
                break;
            case TRISTE:
                if(is_frame_1)
                    avatar.setImageResource(avatar_frame[2][0]);
                else
                    avatar.setImageResource(avatar_frame[2][1]);

                is_frame_1 = !is_frame_1;

                break;
            case MUERTO:
                if(is_frame_1)
                    avatar.setImageResource(avatar_frame[3][0]);
                else
                    avatar.setImageResource(avatar_frame[3][1]);

                is_frame_1 = !is_frame_1;
                break;
        }
    }
}
